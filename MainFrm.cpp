//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainFrm.h"
#include <math.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

int MoveFlag = 0;
int PrevHolderPos = 0;
int PrevSliderPos = 0;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
        MoveFlag = 1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1MouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
        MoveFlag = 0;        
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Image1MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
        if(MoveFlag != 0)
        {
                Form1->Caption = X;
        };
}
//---------------------------------------------------------------------------
void __fastcall TForm1::HolderScrollScroll(TObject *Sender,
      TScrollCode ScrollCode, int &ScrollPos)
{
        HolderPic->Left = -(ScrollPos);
        SliderPic->Left = SliderPic->Left + (HolderPic->Left - PrevHolderPos);
        PrevHolderPos = HolderPic->Left;
        
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
        HolderScroll->Max = HolderPic->Picture->Width - Form1->Width;
        HolderScroll->Position = 0;
        SliderScroll->Max = SliderPic->Picture->Width - Form1->Width;
        SliderScroll->Min = -(SliderPic->Picture->Width - Form1->Width);
        SliderScroll->Position = 0;
        PrevHolderPos = HolderPic->Left;
        // HolderPic->Canvas->Pen->Color = (TColor)RGB(255,0,0);
        // HolderPic->Canvas->MoveTo(HolderPic->Width/2,0);
        // HolderPic->Canvas->LineTo(HolderPic->Width/2,HolderPic->Height);

}
//---------------------------------------------------------------------------

void __fastcall TForm1::SliderScrollScroll(TObject *Sender,
      TScrollCode ScrollCode, int &ScrollPos)
{
        SliderPic->Left = ScrollPos;
        PrevSliderPos = SliderPic->Left;
}

int OldSliderX = 0;
int OldHolderX = 0;
bool SliderMouseDown = false;
bool HolderMouseDown = false;


//---------------------------------------------------------------------------
void __fastcall TForm1::SliderPicMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
        // if(SliderMouseDown == true)
        //        if(X > OldSliderX) SliderPic->Left = SliderPic->Left + abs(X - OldSliderX);
        //        else if(X < OldSliderX) SliderPic->Left = SliderPic->Left - abs(X - OldSliderX);
        // OldSliderX = X;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SliderPicMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
        SliderMouseDown = true;        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SliderPicMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
        SliderMouseDown = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::HolderPicMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
        HolderMouseDown = true;        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::HolderPicMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
        HolderMouseDown = false;        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::HolderPicMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
        // if(HolderMouseDown == true)
        //        if(X > OldHolderX) HolderPic->Left = HolderPic->Left + abs(X - OldHolderX);
        //        else if(X < OldHolderX) HolderPic->Left = HolderPic->Left - abs(X - OldHolderX);
        // OldHolderX = X;

}
//---------------------------------------------------------------------------

